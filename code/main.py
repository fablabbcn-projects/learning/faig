import folium
import csv
import pandas as pd
import geopandas as gpd
import numpy as np
from folium.plugins import MarkerCluster
from folium.plugins import StripePattern
from IPython.display import HTML



nom=[]
data_lon=[]
data_lat=[]
poblacio=[]
color=[]
tipus=[]

# Load the data
y=0
with open("xarxafaig.csv") as rtsd:
    read=csv.reader(rtsd,delimiter=",")
    for i in read:
            nom.append(i[0])
            poblacio.append(i[2])
            data_lon.append(i[7])
            data_lat.append(i[8])
            color.append(i[9])
            tipus.append(i[10])


#for skiping columns name
nom=nom[1:]
data_lon=data_lon[1:]
data_lat=data_lat[1:]
poblacio=poblacio[1:]
color=color[1:]
tipus=tipus[1:]

map = folium.Map([41.4506304,2.0913448], zoom_start=11,tiles="Stamen Toner")
tile = folium.TileLayer('Stamen Terrain').add_to(map)

ly1 = MarkerCluster(name='Centres Educatius').add_to(map)
ly2 = MarkerCluster(name='Espais Maker').add_to(map)

#adding marker and popup of city and crime-name
for i in range(0,len(nom)):
    if tipus[i] in ["Escola"]:
        folium.Marker(location=[float(data_lon[i]),float(data_lat[i])], icon=folium.Icon(color=color[i],icon_color='#FFFFFF'),popup="<b>"+nom[i]+"</b>\n"+poblacio[i]).add_to(ly1)
    else:
        folium.Marker(location=[float(data_lon[i]), float(data_lat[i])],
                      icon=folium.Icon(color=color[i], icon_color='#FFFFFF'),
                      popup="<b>" + nom[i] + "</b>\n" + poblacio[i]).add_to(ly2)

#we can change tiles with help of LayerConrol
folium.LayerControl().add_to(map)

#saving map to a html file
map.save('index.html')
