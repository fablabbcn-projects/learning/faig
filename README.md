# Comunitat Faig

**El Mapa de La Comunitat FAIG** visualitza la xarxa d'entitats participants al projecte.

[Fes click aquí per navegar sobre el mapa.](https://fablabbcn-projects.gitlab.io/learning/faig/public/)

![imagen-1.png](./imagen.png)

FAIG és un projecte de [CESIRE](https://serveiseducatius.xtec.cat/cesire/?_ga=2.4783987.1579052303.1664913233-883197285.1610094632) amb el suport de [IAAC Fab Lab Barcelona](https://fablabbcn.org/).

Per qualsevol dubte podeu contactar amb [xavi@fablabbcn.org](mailto://xavi@fablabbcn.org) o [santi@fablabbcn.org](mailto://santi@fablabbcn.org)
